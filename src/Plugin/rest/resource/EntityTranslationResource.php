<?php

namespace Drupal\rest_translation\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Represents entities as resources.
 */
class EntityTranslationResource extends EntityResource {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
   $instance = parent::create(
     $container,
     $configuration,
     $plugin_id,
     $plugin_definition
   );

    $instance->requestStack = $container->get('request_stack');

    return $instance;
  }

  /**
   * Responds to entity GET requests.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the entity with its accessible fields.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(EntityInterface $entity, Request $request) {
    if ($langcode = $this->getRequestedTranslationLangcode($request)) {
      $entity = $this->getRequestedTranslationEntity($entity, $langcode);
      if (!$entity) {
        throw new NotFoundHttpException("No translation found for language '" . $langcode . "'");
      }
    }

    // We need to add cache context for query params.
    $response = parent::get($entity, $request);
    $response->getCacheableMetadata()->addCacheContexts(['url.query_args']);

    return $response;
  }

  /**
   * Responds to entity POST requests and saves the new entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post(EntityInterface $entity = NULL) {
    // When creating a new entity, let the normal process handle the language.
    // Usually the language code will define the language of the created entity.
    return parent::post($entity);
  }

  /**
   * Responds to entity PATCH requests.
   *
   * @param \Drupal\Core\Entity\EntityInterface $original_entity
   *   The original entity object.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL) {
    if ($original_entity instanceof TranslatableInterface && $original_entity->isTranslatable()) {
      if ($langcode = $this->getRequestedTranslationLangcode($this->requestStack->getMainRequest())) {
        $original_entity = $original_entity->hasTranslation($langcode) ?
          $original_entity->getTranslation($langcode) : $original_entity->addTranslation($langcode);
      }
    }
    return parent::patch($original_entity, $entity);
  }

  /**
   * Responds to entity DELETE requests.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function delete(EntityInterface $entity) {
    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if ($langcode = $this->getRequestedTranslationLangcode($this->requestStack->getMainRequest())) {
        if ($entity->hasTranslation($langcode)) {
          // If it's not the default translation, we remove the translation.
          if (!$entity->isDefaultTranslation()) {
            try {
              $entity->removeTranslation($langcode);
              $entity->save();
              return new ModifiedResourceResponse(NULL, 204);
            }
            catch (EntityStorageException $e) {
              throw new HttpException(500, 'Internal Server Error', $e);
            }
          }
          // Otherwise let the normal process go on and delete the entire
          // entity.
          else {
            return parent::delete($entity);
          }
        }
        else {
          throw new NotFoundHttpException("No translation found for language '" . $langcode . "'");
        }
      }
    }
    return parent::delete($entity);
  }

  /**
   * Returns the requested translation for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param string $langcode
   *   The language code.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|false
   *   The entity object or FALSE if not found.
   */
  protected function getRequestedTranslationEntity(EntityInterface $entity, string $langcode) {
    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if ($entity->hasTranslation($langcode)) {
        return $entity->getTranslation($langcode);
      }
    }

    return FALSE;
  }

  /**
   * Returns the requested translation language code.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   *
   * @return string|null
   *   The language code, or NULL if not found.
   */
  protected function getRequestedTranslationLangcode(Request $request): ?string {
    if ($request->query->get('_translation') != '') {
      return $request->query->get('_translation');
    }

    return NULL;
  }

}
