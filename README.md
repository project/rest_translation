# REST Translation

## Description

This module extends Drupal core's Drupal\rest\Plugin\rest\resource\EntityResource
class by providing an additional query param to target the desired translation.

It tries to fulfill the gap of poor multilingual support with REST.

See https://www.drupal.org/docs/8/core/modules/rest/translations

## Usage

Use the `_translation` query param and add the desired langcode.
This query param is supported for the following methods:
- GET
- PATCH
- DELETE

Example: `https://localhost/node/123?_format=json&_translation=fr`

If no `_translation` query param is added to the URL, then the default behavior
of REST module applies.

If no translation is found for the requested entity, then the module will return
and `NotFoundHttpException`.

## Use cases

- Getting an entity in a specific language
- Adding or deleting a specific translation for an entity
